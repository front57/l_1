(function(){
    const images = document.querySelectorAll('.main__img-left, .main__img-right, .main__img-center');
    const cb = (entries, observer) => {
        entries.forEach(item => {
            if(item.isIntersecting){
                item.target.classList.add('fadeIn');
            }
        })
    }
    const opts = {};
    const obs = new IntersectionObserver(cb, opts);

    images.forEach(img => {
        obs.observe(img);
    });
})();

(function(){
    let sleep = 400;
    const items = document.querySelectorAll('.main__title, .main__text, .main__btn-wrapper'),
     cb = (entries, observer) => {
        entries.forEach((item, i) => {
            if(item.isIntersecting){
                setTimeout(function (){
                    item.target.classList.add('fadeIn');
                }, sleep);
            }
            sleep += 200;
        });
    };
    const opts = {};
    const obs = new IntersectionObserver(cb, opts);

    items.forEach(item => {
        obs.observe(item);
    });
})();

(function(){
    let sleep = 200;
    const items = document.querySelectorAll(' .anim__to-left, .anim__to-right, .anim__to-top'),
        cb = (entries, observer) => {
            entries.forEach((item, i) => {
                if(item.isIntersecting){
                    setTimeout(function (){
                        item.target.classList.add('fadeIn');
                    }, sleep);
                }
                sleep += 300;
                if(i % 3 === 0){sleep=200;}
            });
        };
    const opts = {};
    const obs = new IntersectionObserver(cb, opts);

    items.forEach(item => {
        obs.observe(item);
    });
})();