/*const leftBtn = document.getElementById('left'),
    rightBtn = document.getElementById('right');*/
const slides = document.querySelectorAll('.slider__item'),
    dots = document.querySelectorAll('.dots');

let index = 0;

const activeSlide = n => {
    for (const elem of slides) {
        elem.classList.remove('active');
    }
    slides[n].classList.add('active');
}

const activeDot = n => {
    for (const dot of dots) {
        dot.classList.remove('active');
    }
    dots[n].classList.add('active');
}

const prepareSlide = idx => {
    activeSlide(idx);
    activeDot(idx);
}

const nextSlide = () => {
    if (index === slides.length - 1){
        index = 0;
        prepareSlide(index);
    }else{
        index++;
        prepareSlide(index);
    }
}

const prevSlide = () => {
    if (index === 0){
        index = slides.length-1;
        prepareSlide(index);
    }else{
        index--;
        prepareSlide(index);
    }
}

dots.forEach((item, indexDot)=>{
    item.addEventListener('click', () => {
        index = indexDot;
        prepareSlide(index);
    });
})

/*leftBtn.addEventListener('click', prevSlide);
rightBtn.addEventListener('click', nextSlide);*/

setInterval(nextSlide, 5000);