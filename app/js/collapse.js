let coll = document.getElementsByClassName("collapsible");
let collContent = document.getElementsByClassName("collapse-content");

for (let i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        collContent[i].classList.toggle("active1");
        let content = this.nextElementSibling;
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
        switchCollapseBtn(this.children);
    });
}

function switchCollapseBtn(elems){
    let elemArr = Array.from(elems);
    for (const elemsKey in elemArr) {
        if(elems[elemsKey].children.length > 0){
            for (let i = 0;i < elems[elemsKey].children.length;i++){
                elems[elemsKey].children[i].classList.toggle('active');
            }
        }
    }
}

